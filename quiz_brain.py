import random


class QuizBrain:
    def __init__(self, question_list):
        self.question_number = 0
        self.question_list = question_list
        self.score = 0

    def check_answer(self, user_answer, correct_answer):
        if user_answer.lower() == correct_answer.lower():
            print("You got it right")
            self.score += 1
        else:
            print("You got it wrong")
        print(f"The correct answer was {correct_answer}")
        print(f"Your current score is {self.score}/{self.question_number}")
        print("\n")

    def next_question(self):
        self.question_number += 1
        random_question = random.choice(self.question_list)
        self.question_list.pop(self.question_list.index(random_question))
        user_answer = input(
            f"Q.{self.question_number} {random_question.text} (True/False): ")
        self.check_answer(user_answer, random_question.answer)

    def still_has_questions(self):
        if len(self.question_list):
            return True
        else:
            return False

    def final_score(self):
        print("You've completed the quiz")
        print(f"Your final score is {self.score}/{self.question_number}")
