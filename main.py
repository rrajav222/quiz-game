import requests
#from data import question_data
from question_model import Question
from quiz_brain import QuizBrain

print("Welcome to Quiz Show")
difficulty = input("Choose difficulty: (easy/medium/hard) ").lower()
#number_of_questions = int(input("Choose number of questions: "))
category = input("Enter category (General/Sports/History/Geography)").lower()

if category == "general":
    category_code = 9
elif category == "sports":
    category_code = 21
elif category == "history":
    category_code = 23
elif category == "geography":
    category_code = 22

response = requests.get(
    f"https://opentdb.com/api.php?amount=10&category={category_code}&difficulty={difficulty}&type=boolean"
)
print("\n")

if response.json()["results"]:

    question_bank = []
    for item in response.json()["results"]:
        question_text = item["question"]
        question_answer = item["correct_answer"]
        new_question = Question(question_text, question_answer)
        question_bank.append(new_question)

    quiz = QuizBrain(question_bank)

    while quiz.still_has_questions():
        quiz.next_question()
    quiz.final_score()

else:
    print("Oops! Quiz not available")
